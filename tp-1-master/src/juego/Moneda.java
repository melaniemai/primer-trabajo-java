package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Moneda {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Image moneda;

	public Moneda(double x, double y) {
		this.x = x;
		this.y = y;
		this.alto = 30;
		this.ancho = 24;
		this.moneda = Herramientas.cargarImagen("moneda.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(moneda, x, y, 0);
	}

	public void mover() {
		x -= 1;
	}

	public boolean esVisible() {
		return x > -50;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}

	public double getAlto() {
		return alto;
	}

}
