package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Obstaculo {

	private double x;
	private double y;

	private double alto;
	private double ancho;
	private double escala;
	private double velocidad;

	private Image cactus;

	public Obstaculo(double x, double y, double velocidad, double escala) {
		this.alto = 86; // pixeles del cactus
		this.ancho = 70;
		this.x = x;
		this.y = y;
		this.velocidad = velocidad;
		this.escala = escala;
		this.cactus = Herramientas.cargarImagen("Cactus.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(cactus, x, y, 0, escala);
	}

	public void mover(Entorno entorno) {
		if (x + ancho / 2 < 0) {
			x = entorno.ancho() + 200;
			return;
		}
		x -= velocidad;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAlto() {
		return alto;
	}

	public double getAncho() {
		return ancho;
	}

}
