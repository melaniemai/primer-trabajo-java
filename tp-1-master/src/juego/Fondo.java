package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Fondo {
	private double x;
	private double y;
	private double velocidad;
	private double tamanio;
	private Image objeto;

	
	public Fondo (double x, double y, double velocidad, String imagen, double tamanio) {
		this.x = x;
		this.y = y;
		this.velocidad = velocidad;
		this.objeto = Herramientas.cargarImagen(imagen);
		this.tamanio = tamanio;
	}
	

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(objeto, x, y, 0, tamanio);
	}

	public void mover(Entorno entorno) {
		if (x < -100) {
			x = entorno.ancho() + 200;
			return;
		}
		x -= velocidad;
	}
}
