package juego;

import java.awt.Image;
import java.util.ArrayList;

import entorno.Entorno;
import entorno.Herramientas;

public class Princesa {
	private double x;
	private double y;

	private double alto;
	private double ancho;

	private double velocidad;
	private double velocidadSalto;
	private double gravedad;

	private Image princesa;
//	private String imagenActual;

	private boolean estaEnElPiso;
	private boolean miraDerecha;

	private int maximaCantidadDeDisparos;
	private int vida;

	private ArrayList<Proyectil> proyectiles;

	public Princesa(double x, double y, double velocidad) {
		this.x = x;
		this.y = y;
		this.alto = 150;
		this.ancho = 90;
		this.velocidad = velocidad;
		this.velocidadSalto = 5.9;
		this.princesa = Herramientas.cargarImagen("princesamov.gif");
		this.miraDerecha = true;
		this.estaEnElPiso = true;
//		this.imagenActual = "";
		this.proyectiles = new ArrayList<Proyectil>();
		this.maximaCantidadDeDisparos = 10;
		this.gravedad = 4.9;
		this.vida = 3;

	}

	public void invencible() {
		cambiarImagen("princesaLastimada.gif");
	}

	public void dibujar(Entorno entorno, int tiempo) {
		if (estaEnElPiso && tiempo <= 0) {
			if (!miraDerecha) {
				cambiarImagen("princesa0movreversa.gif");
			} else {
				cambiarImagen("princesamov.gif");
			}
		} else if (!estaEnElPiso && tiempo <= 0) {
			if (miraDerecha) {
				cambiarImagen("princesaSalto.png");
			} else {
				cambiarImagen("princesaSaltoreversa.png");
			}
		} else {
			if (miraDerecha) {
				cambiarImagen("princesaLastimada.gif");
			} else {
				cambiarImagen("princesaLastimadareversa.gif");
			}
		}
		entorno.dibujarImagen(princesa, x, y, 0);
	}

//	public void lastimada(String s) {
//		cambiarImagen(s);
//	}

//	private void cambiarImagen(String imagen) {
//		if (!imagenActual.equals(imagen)) {
//			princesa = Herramientas.cargarImagen(imagen);
//			imagenActual = imagen;
//		}
//	}

	private void cambiarImagen(String imagen) {
		Image img = Herramientas.cargarImagen(imagen);
		if (princesa != img) {
			princesa = img;
		}
	}
	
	public void moverIzquierda() {
		miraDerecha = false;
		// un pequenio margen con el que choca
		if (x > 60) {
			this.x -= this.velocidad;
		}
	}

	public void moverDerecha() {
		miraDerecha = true;
		// antes de que termine la pantalla
		if (x < 750) {
			this.x += this.velocidad;
		}
	}

	public void saltar() {
		this.estaEnElPiso = false;
	}

	public ArrayList<Proyectil> getProyectiles() {
		return proyectiles;
	}

//	public void mostrarCantidadDisparos(Entorno entorno) {
//		String mostrarMunicion = Integer.toString(maximaCantidadDeDisparos);
//	}

	public void lanzarFuego() {
		if (maximaCantidadDeDisparos > 0) {
			proyectiles.add(new Proyectil(x, y));
			maximaCantidadDeDisparos--;
		}
	}

	public boolean chocasteConObstaculo(Obstaculo o) {
		return x >= o.getX() - o.getAncho() + 5 && x <= o.getX() + o.getAncho() - 5
				&& y + ancho / 2 > o.getY() - o.getAlto() / 2;
	}

	public boolean chocasteConEnemigo(Enemigo e) {
		return x >= e.getX() - e.getAncho() && x <= e.getX() + e.getAncho()
				&& y + ancho / 2 > e.getY() - e.getAlto() / 2;
	}

	public boolean agarrasteMoneda(Moneda m) {
		return x + (ancho / 2) >= m.getX() - m.getAncho() / 2 + 5 && x - (ancho / 2) <= m.getX() + m.getAncho() / 2 - 5
				&& y - (alto / 2) <= m.getY() + m.getAlto() - 5 && y + (alto / 2) >= m.getY() - m.getAlto() + 5;
	}

	public int saltarYCaer(int altura) {
		if (!estaEnElPiso) {
			if (altura <= 34) {
				y -= velocidadSalto;
				altura++;
			} else if (altura >= 34 && altura < 78 && y <= 420) {
				y += gravedad;
				altura++;
			} else {
				estaEnElPiso = true;
				altura = 0;
			}
		}
		return altura;
	}

	public void restarVida() {
		vida--;
	}

	public void sumarMunicion() {
		maximaCantidadDeDisparos += 2;
	}

	public double getVelocidadSalto() {
		return velocidadSalto;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int getVida() {
		return vida;
	}

	public int getMaximaCantidadDeDisparos() {
		return maximaCantidadDeDisparos;
	}

	public boolean estaViva() {
		return vida > 0;
	}

	public boolean estaEnElPiso() {
		return estaEnElPiso;
	}

}
