package juego;

import java.awt.Image;
import java.util.ArrayList;

import entorno.Entorno;
import entorno.Herramientas;

public class Enemigo {
	private double x;
	private double y;
	private double alto; 
	private double ancho;
	private double velocidad;
	
	private Image enemigo;
	private boolean estaMuerto;

	public Enemigo(double x, double y) {
		this.x = x;
		this.y = y;
		this.alto = 150;
		this.ancho = 110;
		this.velocidad = 1;
		this.estaMuerto = false;

		this.enemigo = Herramientas.cargarImagen("enemigo.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(enemigo, x, y, 0, 0.255);
	}

	public void mover() {
		x -= velocidad;
	}

	public boolean ocupaMismaPosicion(ArrayList<Enemigo> enemigos) {
		for (Enemigo e : enemigos) {
			if (x >= e.getX() - e.getAncho() && x <= e.getX() + e.getAncho()) {
				return true;
			}
		}
		return false;

	}

	public void matar() {
		estaMuerto = true;
	}

	public boolean estaVivo() {
		return estaMuerto;
	}

	public boolean esVisible() {
		return x > -50;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAlto() {
		return alto;
	}

	public double getAncho() {
		return ancho;
	}

}
