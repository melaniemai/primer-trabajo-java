package juego;

import java.awt.Color;

import java.util.ArrayList;

import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {
	
	private Entorno entorno;
	private Fondo cielo;
	private Fondo muestraMonedita;
	private Fondo muestraEnemigosMuertos;

	private Fondo pasto1;
	private Fondo fuegoArtificial;
	private Fondo fuegoArtificial2;
	private Fondo cartelPerdiste;

	private Princesa princesa;
	private ArrayList<Enemigo> enemigos;
	private ArrayList<Obstaculo> obstaculos;
	private ArrayList<Fondo> nubes;
	private ArrayList<Moneda> monedas;

	private int altura;
	private int tiempoOff;
	private int enemigosMuertos;
	private int puntaje;
	private int monedasAgarradas;

	public Juego() {

		entorno = new Entorno(this, "Princesa Elizabeth Sis", 800, 600);

		cielo = new Fondo(entorno.ancho() / 2, entorno.alto() / 2, 0, "fondoCeleste.png", 1.01);
		pasto1 = new Fondo(entorno.ancho() / 2, entorno.alto() - 57, 0, "pasto1.png", 1.1);

		muestraMonedita = new Fondo(entorno.ancho() / 2 - 40, 50, 0, "moneda.png", 0.50);
		muestraEnemigosMuertos = new Fondo(630, 60, 0, "enemigo.png", 0.07);

		nubes = new ArrayList<Fondo>();
		nubes.add(new Fondo(entorno.ancho() - 150, entorno.alto() - 495, 0.8, "nube1.png", 1.1));
		nubes.add(new Fondo(entorno.ancho() + 450, entorno.alto() - 385, 1.5, "nube2.png", 1.2));
		nubes.add(new Fondo(entorno.ancho() * 2, entorno.alto() - 455, 1.2, "nube3.png", 1.3));

		princesa = new Princesa(entorno.ancho() - 750, entorno.alto() - 185, 5);
		enemigos = new ArrayList<Enemigo>();

		obstaculos = new ArrayList<Obstaculo>();
		obstaculos.add(new Obstaculo(entorno.ancho() + 200, 490 - 36, 2.5, 0.9));
		obstaculos.add(new Obstaculo(entorno.ancho() + 400, 490 - 44, 2, 1.10));

		monedas = new ArrayList<Moneda>();
		monedas.add(new Moneda(400, 300));

		fuegoArtificial = new Fondo(entorno.ancho() / 2, 200, 0, "fuegosArtificiales.gif", 1);
		fuegoArtificial2 = new Fondo(entorno.ancho() - 100, 400, 0, "fuegosArtificiales.gif", 1);
		cartelPerdiste = new Fondo(entorno.ancho() / 2, entorno.alto() / 2, 0, "gameOver.gif", 1);

		altura = tiempoOff = enemigosMuertos = puntaje = monedasAgarradas = 0;

		this.entorno.iniciar();
	}

	public boolean ganaste() {
		return enemigosMuertos >= 20 || puntaje >= 200;
	}

	public void tick() {

		cielo.dibujar(entorno);
		pasto1.dibujar(entorno);

		muestraMonedita.dibujar(entorno);
		muestraEnemigosMuertos.dibujar(entorno);

		entorno.cambiarFont("Consolas", 25, Color.WHITE);
		entorno.escribirTexto("SCORE " + Integer.toString(puntaje), 20, 30);

		entorno.cambiarFont("Consolas", 25, Color.WHITE);
		entorno.escribirTexto("VIDA " + Integer.toString(princesa.getVida()), 350, 30);

		entorno.cambiarFont("Consolas", 25, Color.WHITE);
		entorno.escribirTexto("x " + Integer.toString(monedasAgarradas), entorno.ancho() / 2 - 28, 56);

		entorno.cambiarFont("Consolas", 25, Color.WHITE);
		entorno.escribirTexto("x " + Integer.toString(enemigosMuertos), 650, 66);

		entorno.cambiarFont("Consolas", 25, Color.WHITE);
		entorno.escribirTexto("PROYECTILES " + Integer.toString(princesa.getMaximaCantidadDeDisparos()), 600, 30);

		for (Fondo n : nubes) {
			n.dibujar(entorno);
		}
		for (Obstaculo o : obstaculos) {
			o.dibujar(entorno);
		}
		for (Moneda m : monedas) {
			if (m.esVisible()) {
				m.dibujar(entorno);

			} else {
				monedas.remove(m);
			}
			if (monedas.size() < 1) {
				monedas.add(new Moneda(900, 350 + (int) (Math.random() * 100)));
			}
		}

		for (Enemigo e : enemigos) {
			if (e != null) {
				if (e.esVisible() && princesa.estaViva()) {
					e.dibujar(entorno);
					e.mover();
				} else {
					enemigos.remove(e);
				}
			}
		}

		if (enemigos.size() < 3 && !ganaste()) {
			int offSet = 100 + (int) (Math.random() * 1000);
			Enemigo enemigoMomentaneo = (new Enemigo(800 + offSet, entorno.alto() - 180));
			enemigos.add(enemigoMomentaneo);
			while (enemigoMomentaneo.ocupaMismaPosicion(enemigos) && !ganaste()) {
				offSet = 100 + (int) (Math.random() * 1000);
				enemigoMomentaneo = (new Enemigo(800 + offSet, entorno.alto() - 180));
			}
		}

		for (Enemigo e : enemigos) {
			for (Proyectil p : princesa.getProyectiles()) {
				if (p.chocasteConEnemigo(e)) {
					enemigosMuertos++;
					puntaje += 5;
					enemigos.remove(e);
					princesa.getProyectiles().remove(p);
				}
				if (p.isVisible()) {
					p.dibujar(entorno);
					p.mover(entorno);
				} else {
					princesa.getProyectiles().remove(p);
				}
			}
		}
		if (ganaste()) {
			fuegoArtificial2.dibujar(entorno);
			entorno.cambiarFont("Consolas", 75, Color.YELLOW);
			entorno.escribirTexto("G A N A S T E", 140, 300);
			fuegoArtificial.dibujar(entorno);
			for (Obstaculo o : obstaculos) {
				obstaculos.remove(o);
				for (Enemigo e : enemigos) {
					enemigos.remove(e);
				}
			}

		}
		if (!princesa.estaViva()) {
			cartelPerdiste.dibujar(entorno);
		}
		if (princesa.estaViva()) {
			if (entorno.estaPresionada('a') || entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
				princesa.moverIzquierda();
			}

			if (entorno.estaPresionada('d') || entorno.estaPresionada(entorno.TECLA_DERECHA)) {
				princesa.moverDerecha();
			}

			if (entorno.sePresiono('w') || entorno.sePresiono(entorno.TECLA_ARRIBA)) {
				if (princesa.estaEnElPiso()) {
					princesa.saltar();
				}
			}

			if (entorno.estaPresionada('d') || princesa.getX() > 150) {
				for (Fondo n : nubes) {
					n.dibujar(entorno);
					n.mover(entorno);
				}

				for (Obstaculo o : obstaculos) {
					o.dibujar(entorno);
					o.mover(entorno);
					if (princesa.chocasteConObstaculo(o) && tiempoOff == 0) {
						tiempoOff = 100;
						princesa.restarVida();
					}
				}

				for (Moneda m : monedas) {
					m.dibujar(entorno);
					m.mover();
					if (princesa.agarrasteMoneda(m)) {
						monedasAgarradas += 1;
						puntaje += 3;
						princesa.sumarMunicion();
						monedas.remove(m);
					}
					if (monedas.size() < 1) {
						monedas.add(new Moneda(900, 280 + (int) (Math.random() * 150)));
					}
				}
			}

			if (tiempoOff > 0) {
				princesa.invencible();
				tiempoOff--;
			}

			for (Enemigo e : enemigos) {
				if (princesa.chocasteConEnemigo(e) && tiempoOff <= 0) {
					tiempoOff = 200;
					princesa.restarVida();
				}
			}
			princesa.dibujar(entorno, tiempoOff);

			if (entorno.sePresiono(entorno.TECLA_ESPACIO)) {
				princesa.lanzarFuego();
			}

			altura = princesa.saltarYCaer(altura);
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
