package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Proyectil {
	private double x;
	private double y;
	private double ancho;
	private double velocidad;

	private Image proyectil;
	public boolean visible;

	public Proyectil(double xInicial, double yInicial) {
		this.x = xInicial;
		this.y = yInicial;
		this.ancho = 60;
		this.velocidad = 1;
		this.proyectil = Herramientas.cargarImagen("boladefuego.png");
		this.visible = true;
	}

	public boolean isVisible() {
		return visible;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(proyectil, x, y, 0);
	}

	public void mover(Entorno entorno) {
		x += velocidad * 3;
		if (x > entorno.ancho()) {
			visible = false;
		}
	}

	public boolean chocasteConEnemigo(Enemigo e) {
		return e.getX() - e.getAncho() + 40 <= this.x && this.x <= e.getX() + e.getAncho() - 40
				&& this.y + ancho / 2 > e.getY() - e.getAlto() / 2;
	}

}
